package me.waitlist.core
package http

import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Base servlet trait for managing cron endpoints.
 * 
 * @author dsumera
 */
abstract class DispatchServlet extends HttpServlet {
  
  override def doGet(req: HttpServletRequest, res: HttpServletResponse) =
    dispatch(req.getPathInfo.substring(1))
    
  /**
   * Implemented by derived classes.
   */
  def dispatch: String PartialFunction Unit
}