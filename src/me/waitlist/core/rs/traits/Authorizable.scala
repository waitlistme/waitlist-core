package me.waitlist.core.rs
package traits

import javax.inject.Inject
import com.google.appengine.repackaged.org.apache.commons.codec.digest.DigestUtils
import me.waitlist.core.rs.exception.NotAuthorizedException
import me.waitlist.core.db.Place
import me.waitlist.core.dao.UserDao
import me.waitlist.core.rs.exception.NotFoundException
import me.waitlist.core.db.User
import me.waitlist.core.dao.crit.EQCriterion
import me.waitlist.core.actor.MailActor
import me.waitlist.core.actor.MailActor.SendError

/**
 * Trait for JAX-RS resources requiring authentication-based functionality.
 *
 * @author dsumera
 */
trait Authorizable {

  // type alias to simplify java map conversions
  type JavaMap = java.util.Map[String, _]
  
  /**
   * Login with credentials and return associated <code>User</code> key.
   */
  def login(email: String, pass: String): User = if (email.isEmpty || pass.isEmpty) {
    val exc = new NotAuthorizedException("invalid user credentials")
    mail ! SendError(exc)
    throw exc
  } else userDao.find(EQCriterion("email", email))( /* no loads */ )._1 match {
    // check the user password
    case user :: _ =>
      // password hash has format "<alg>$<salt>$<hash>"
      user.password.split("\\$").toList match {
        // validate password and return user model
        case List(alg, salt, hash) => alg match {
          case "sha1" => if (DigestUtils.sha1Hex(s"$salt$pass".getBytes) == hash) user
          else {
            val exc = new NotAuthorizedException("invalid user credentials")
            mail ! SendError(exc)
            throw exc
          }

          // add other decryption algorithms here
          // case "crypt" =>
          // case "md5" =>
        }

        case _ =>
          val exc = new IllegalArgumentException("invalid account hash")
          mail ! SendError(exc)
          throw exc
      }

    // no accounts with given email have been found
    case Nil =>
      val exc = new NotAuthorizedException("invalid user credentials")
      mail ! SendError(exc)
      throw exc
  }

  /**
   * Check that the api key authorizes access to the given place.
   */
  def ifAuthorized[R](place: Long, key: String)(execute: => R) =
    userDao.find(EQCriterion("api_key", key), EQCriterion("place_ids", place))( /* no loads */ )._1 match {
      // no authorized users found for given key and place
      case Nil =>
        val exc = new NotFoundException(s"no key:$key with access to place:$place exists")
        mail ! SendError(exc)
        throw exc

      // found user, execute by-name parameter (i.e., no-arg function)
      case _ => execute
    }

  /**
   * Closure which determines if <code>Place</code> eligible for API access. Changed this method
   * according to desired tiered pricing.
   */
  def isApiAllowed(place: Place) = place.is_beta || place.is_premium || place.is_pro
  
  @Inject var userDao: UserDao = _
  
  @Inject private[this] var mail: MailActor = _
}