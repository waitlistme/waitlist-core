package me.waitlist.core.rs
package ext

import javax.inject.Singleton
import javax.ws.rs.ext.{ContextResolver, Provider}

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.googlecode.objectify.util.jackson.ObjectifyJacksonModule

/**
 * Object mapper for generating serialization of object to desired format (JSON).
 *
 * @author dsumera
 */
@Singleton
@Provider
class ObjectMapperProvider extends ContextResolver[ObjectMapper] {

  // use initialization holder to return singleton instance of provider
  override def getContext(clazz: Class[_]) = ObjectMapperProvider.mapper
}

object ObjectMapperProvider {

  val mapper = (new ObjectMapper)
    // required to prevent cycles during JSON serialization due to keys
    .registerModule(new ObjectifyJacksonModule)
    // suppress failures when deserializing unknownn properties
    .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
}