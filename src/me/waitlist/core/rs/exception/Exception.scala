package me.waitlist.core.rs
package exception

import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.Response
import scala.collection.mutable.ListBuffer
import javax.ws.rs.core.Response.Status
import scala.collection.JavaConversions._

//object CustomWebApplicationException {
//  /**
//   * Implicit class for exception folding.
//   */
//  implicit class Exceptionable(var err: Option[CustomWebApplicationException]) {
//
//    /**
//     * Add an exception to an existing one.
//     */
//    def +=(exc: CustomWebApplicationException) = err match {
//      // combine existing errors
//      case Some(e) => e.errors ++= exc.errors; this.err = err
//
//      // assign exception
//      case None => err = Option(exc); this.err = err
//    }
//  }
//}

abstract class CustomWebApplicationException(error: String, status: Status)
    extends WebApplicationException {

  /** errors for exception condition */
  val errors = ListBuffer[String]( /* empty */ )

  // add constructor provided error
  errors += error

  /**
   * Add additional errors to the original exception
   */
  //def +=(error: String) = { errors += error; this }

  /**
   * Add errors from existing exception to this exception.
   */
  def +=(e: CustomWebApplicationException) = { errors ++= e.errors; this }
  
  override def getResponse = Response.status(status)
    //.entity(Array(errors.toSeq: _*))
    .entity(Map("errors" -> Array(errors.toSeq: _*)): java.util.Map[String, _])
    .build
}

/**
 * Custom exception indicating accessed resource is forbidden.
 */
class NotAuthorizedException(error: String) extends CustomWebApplicationException(error, Status.FORBIDDEN)

/**
 * Custom exception indicating requested resource not found.
 */
class NotFoundException(error: String) extends CustomWebApplicationException(error, Status.NOT_FOUND)

class QueryParamException(error: String, param: String, value: String)
  extends CustomWebApplicationException(error, Status.INTERNAL_SERVER_ERROR) 
{
    //extends com.sun.jersey.api.ParamException.QueryParamException(t, param, value) {
  //val errors = List(s"${t.getMessage}")
  //override def getResponse = Response.status(Status.INTERNAL_SERVER_ERROR)
  //  .entity(Map("errors" -> Array(errors: _*)): java.util.Map[String, _])
  //  .build
}