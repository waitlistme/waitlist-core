package me.waitlist.core.db;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * Overall <code>Stats</code> data.
 *
 * @author dsumera
 */
@Entity
public class OverallStats extends Stats implements Serializable {

    @Id public String id;

    public OverallStats() {
    	/* default no-arg constructor */
    }

    public OverallStats(String id) {
    	this.id = id;
    }
    
    /**
     * last updated time, not indexed
     */
    public Date last_updated;
}