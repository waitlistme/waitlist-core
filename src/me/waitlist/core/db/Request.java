package me.waitlist.core.db;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;

/**
 * Tracking requests against remotely requested <code>Party</code>.
 * 
 * @author dsumera
 */
@Entity(name = "PartyRequest")
public class Request implements Serializable {

	@Id public Long id;
	
    /** created date */
    @Index public Date added_date = new Date();
    
    /** modified date */
    @Index public Date modified_date = new Date();
    
    @Index public Ref<Customer> customer;
    
	@OnSave
	private void update() {
		// set a new modified date
		modified_date = new Date();
	}
	
//customer
//=	Key	
//Key(Customer, 1102288)
//
//
//
//modified_date
//=	Date and time	7/23/13, 7:35 AM EDT	
//
//
//
//name
//=	String	
//G
//
//
//
//party_request_max_duration
//=	Date and time	7/23/13, 7:45 AM EDT	
//
//
//
//place
//=	Key	
//Key(Place, 25178697)
//
//
//
//size
//=	Integer	
//1
//
//
//
//status
//=	String	
//accepted


}