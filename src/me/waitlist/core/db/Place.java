package me.waitlist.core.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.google.appengine.api.datastore.Email;
import com.google.appengine.api.datastore.PhoneNumber;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Unindex;

/**
 * Entity for capturing a business for which a waitlist is maintained.
 *
 * @author dsumera
 */
@Entity
//@Cache
public class Place implements Serializable {

	@Id public Long id;

	// place information
	
	@Index public Email email;
	@Index public String name;
	@Index public String name_lower;
	@Index public String custom_url;
	@Index public String industry_info;
	public String image_url;
	public Text description;
	
	// created date
	@Index public Date added_date = new Date();
	
	// updated every modification
	@Index public Date modified_date;
	
	// might be a string, need to check
	@Index public PhoneNumber phone_number;
	@Index public String premium_phone_number;
	
	// index removed
	public String website;
	
	public Text public_waitlist_html;
	
	// place location
	
	@Index public String address;
	@Index public String city;
	@Index public String region;
	@Index public String country;
	@Index public String postal_code;
	@Index public String latitude = "0.0";
	@Index public String longitude = "0.0";
	
	// index removed
	public String timezone = "US/Pacific";
	
	// place stats
	
	@Index public int seated_last_day = 0;
	@Index public int seated_last_week = 0;
	@Index public int seated_last_month = 0;
	@Index public int total_parties_of_current_month = 0;
	@Index public int total_seated = 0;
	
	// text message formats
	// dsumera: removed indexes since these are not used for anything
	public Text notification_text;
	public Text notification_eta_text;
	public Text removal_notification_text;
	public Text waitlist_add_text;
	public Text waitlist_eta_text;
	public Text waitlist_remove_text = new Text("You have been removed from the waitlist at [name]");

	// place billing
	
	@Index public boolean is_beta = false;
	@Index public boolean is_premium = false;
	@Index public boolean is_pro = false;
	@Index public String appdirect_uid;
	@Index public String clover_id;
	@Index public Date billing_start_time;
	@Index public Date next_billing_date;
	@Index public Date trial_period_date;
	
	// "paid" or "pending"
	@Index public String billing_state;
	@Index public Ref<CreditCard> credit_card;

	@Index public int api_monthly = 0;
	@Index public int mobile_monthly = 0;
	@Index public int web_monthly = 0;
	
	// place settings
	
	// index removed
	public boolean auto_suggestion_feature = true;
	
	// index removed
	// dollar amount shouldn't be searched
	public String custom_amount;
	
	// index removed
	public Date custom_duration;
	
	//gae_extension.JsonProperty()
	public String custom_notes = "{}";
	
	// index removed
	public int customer_name_format = 0;
	
	// index removed
	// db.StringProperty(default='groups',choices=('groups', 'individual'))
	public String customer_type = "groups";
	
	// index removed
	public boolean disable_confirmation_text = false;
	
	// index removed
	public boolean disable_removal_text = true;
	
	// index removed
	// boxed for nullability
	public Boolean disable_customize_msg = false;
	
	// index removed
	public boolean disable_public_waitlist = false;
	
	@Index public boolean enable_resource_management_feature = false;
	
	/** determines whether auto removal deadline is set for submitted party requests */
	// index removed
	public boolean party_request_auto_removal = false;
	
	/** if request auto removal, lifetime before it is removed; otherwise, max party lifetime (i.e., 5 hrs) */
	// index removed
	public int party_request_lifetime_mins = 10;//constants.REMOTE_PARTY_LIFETIME
	
	/** mode of handling when a party request is initiated */
	// index removed
	// db.StringProperty(default='disabled', choices=set(['enabled', 'disabled', 'auto_add']))
	public String party_request_preference = "disabled";

	/** if request preference is auto add, what type of requests should be processed */
	// index removed
	// db.StringProperty(default='waitlist', choices=set(['waitlist', 'reservations', 'both','disabled']))
	public String auto_add_preference = "waitlist";
	
	// index removed
	public boolean party_size_optional = false;
	
	@Index public boolean phone_optional = false;
	
	// index removed
	public String plan;
	
	// index removed
	// db.StringProperty(default='waitlist', choices=set(['waitlist', 'reservations', 'both']))
	public String public_waitlist_preference = "waitlist";
	
	// index removed
	// db.StringProperty(default='reservations',choices=('reservations', 'appointments'))
	public String schedule_type = "reservations";
	
	// index removed
	public boolean show_customize_html = false;
	
	// this was an exploding index that needed to be removed!
	// db.StringListProperty(default=[str(x + 1) for x in range(0, 50)])
	@Unindex public List<String> tables;
	
	// index removed
	public boolean user_controlled_tz = false;
	
	// index removed
	// db.StringProperty(default=constants.SOUND_FILES[13],choices=constants.SOUND_FILES)
	public String voice_call_file = "NL6.mp3";
	
	/** set against added parties, determines if party should be removed after specified deadline */
	// index removed
	public boolean waitlist_auto_removal = true;
	
	// index removed
	// db.StringProperty(default='waiting_time', choices=set(['waiting_time', 'remainging_time']))
	public String waiting_time_display_option = "waiting_time";
	
	// place survey
	
	@Index public boolean survey_notification_enabled = false;
	
	// index removed
	public Text survey_notification_message;
	
	// dsumera: index removed, how many minutes after being seated? nullable in python, need a boxed primitive
	public Integer survey_push_time;
	
	@Index public int survey_total_score = 0;
	@Index public int survey_total_submissions = 0;
	
	//db.TimeProperty(default=datetime.time(9,0,0))
	@Index public Date survey_window_start_time;
	
	// db.TimeProperty(default=datetime.time(21,0,0))
	@Index public Date survey_window_end_time;

	// DEPRECATED: do not use
	// dsumera: at least remove the indexes
	public String address_line1;
	public String url;
	//public String customize_url; // corey: Do we have a mapreduce to migrate this field?
	//public String subdomain;
	//public boolean first_time;

	// an example of a list property, included in query result if single item
	// from property passes all filters for property
	// transient ensures that the property is never included during
	// serialization
	@Index public transient List<String> __searchable_text_index_name = new ArrayList<String>();
	
	public Place() {
		// setting the default survey window (9am to 9pm)
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.setTimeInMillis(0);
		cal.set(Calendar.HOUR_OF_DAY, 9);
		survey_window_start_time = cal.getTime();
		cal.set(Calendar.HOUR_OF_DAY, 21);
		survey_window_end_time = cal.getTime();
		
		// initialize table resources to 1-50
		List<String> tables = new ArrayList<String>();
		for (int i = 0; i < 50; i++)
			tables.add(Integer.toString(i+1));
		this.tables = tables;
	}
	
	@OnSave
	private void update() {
		// set a new modified date
		modified_date = new Date();
		
		// lower-case name
		name_lower = name.toLowerCase();
	}
}