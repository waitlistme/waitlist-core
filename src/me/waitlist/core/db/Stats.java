package me.waitlist.core.db;

import java.io.Serializable;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;

/**
 * Entity for capturing basic stats data.
 *
 * @author dsumera
 */
public abstract class Stats implements Serializable {

    /** place associated with this stat */
    @Index public Ref<Place> place;
    
    public int adult = 0;
    public int adult_females = 0;
    public int adult_males = 0;
    public int child = 0;
    public int child_females = 0;
    public int child_males = 0;
    public int old = 0;
    public int old_females = 0;
    public int old_males = 0;
    public int unknown_persons = 0;

    public int calls_placed = 0;
    public int sms_sent = 0;
    public int table_ready = 0;
    public int table_reminder = 0;

    public int survey_attempts = 0;
    public int survey_ratings = 0;

    /** parties that have been added to list */
    public int waitlist_add = 0;
    
    /** parties that have been removed from list */
    public int waitlist_remove = 0;

    /**
     * Computed property; do not directly modify this field. Use <code>totalRemoved()</code> instead.
     */
    public int total_removed = 0;
    public int total_seated = 0;

    /**
     * Merge statistics with another <code>Stats</code> object.
     *
     * @param stat
     */
    public void merge(Stats stat) {
        // adults
        adult += stat.adult;
        adult_females += stat.adult_females;
        adult_males += stat.adult_males;

        // children
        child += stat.child;
        child_females += stat.child_females;
        child_males += stat.child_males;

        // elderly
        old += stat.old;
        old_females += stat.old_females;
        old_males += stat.old_males;

        // others
        unknown_persons += stat.unknown_persons;

        // actions
        calls_placed += stat.calls_placed;
        sms_sent += stat.sms_sent;
        table_ready += stat.table_ready;
        table_reminder += stat.table_reminder;
        total_seated += stat.total_seated;

        // surveys
        survey_attempts += stat.survey_attempts;
        survey_ratings += stat.survey_ratings;

        // waitlist
        waitlist_add += stat.waitlist_add;
        waitlist_remove += stat.waitlist_remove;
    }

    /**
     * Calculate the total removed before persisting.
     *
     * @return
     */
    @OnLoad
    @OnSave
    public int totalRemoved() {
        total_removed = child + child_males + child_females +
                adult + adult_males + adult_females +
                old + old_males + old_females + unknown_persons - total_seated;
        return total_removed;
    }
}