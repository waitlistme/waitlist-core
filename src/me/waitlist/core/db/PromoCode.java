package me.waitlist.core.db;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/**
 * Promotional codes offered to the <code>User</code>. Referenced from
 * <code>User</code> entity.
 * 
 * @author dsumera
 */
@Entity
public class PromoCode implements Serializable {

	@Id public Long id;
	
	/*
	 *   code_id = db.StringProperty(required=True)
  admin_only = db.BooleanProperty(default=False)
  creation_date = db.DateTimeProperty(auto_now_add=True)
  expire_date = db.DateTimeProperty(required=True)
  free_months = db.IntegerProperty(required=True)
  max_uses = db.IntegerProperty(required=True)
  total_uses = db.IntegerProperty(default=0)
  description = db.StringProperty()

  def is_expired(self):
    return bool(self.expire_date < datetime.datetime.now())

  def reached_max_uses(self):
    return bool(self.total_uses >= self.max_uses)

  def is_valid(self):
    return bool(not self.is_expired() and not self.reached_max_uses())
	 */
}