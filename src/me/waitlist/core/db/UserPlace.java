package me.waitlist.core.db;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
public class UserPlace {

	@Id public Long id;
	
	@Load(User.class)
	@Index public Ref<User> user;
	
	@Load(Place.class)
	@Index public Ref<Place> place;
	
	//db.StringProperty(default='admin', choices=set(['admin', 'user']))
	public String user_type = "admin";
}