package me.waitlist.core.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.appengine.api.datastore.Email;
import com.google.appengine.api.datastore.PhoneNumber;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.IgnoreSave;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.condition.IfDefault;

/**
 * Class mapping to UserProfile (Python). These could probably use some
 * optimization as indexes cost more writes.
 *
 * @author dsumera
 */
@Entity(name = "UserProfile")
//@Cache
public class User implements Serializable {

	@Id public Long id;

	@Index public Email email;
	public transient String password;

	/** associated Google account */
	public com.google.appengine.api.users.User user;

	// dsumera[04/01/16]: authorization key for api usage; don't save if the value is non-existent
	@Index
	@IgnoreSave(IfDefault.class)
	public transient String api_key = null;
	
	/** csr user field */
	@Index public boolean admin = false;
	// user_type = db.StringProperty(default='owner', choices=['owner',
	// 'admin'])
	// hostess can be added over here
	public String user_type = "owner";
	
	/** whether user is enabled or not */
	public boolean blocked = false;

	public String timezone = "US/Pacific";
	@Index public String first_name;
	@Index public String last_name;
	@Index public PhoneNumber phone_number;

	@Index public Date added_date = new Date();
	@Index public Date modified_date = new Date();

	// corey: Do we really need to differentiate last web vs mobile app login?
	public Date last_login;
	public Date last_mobi_login;

	/** local place access list; also can use UserPlace table */
	@Index public List<Long> place_ids;

	// consider moving these to a BillingInfo model assoc w/ a place and user

	// dsumera[03/30/16]: boxed for compatibility
	// billing_failure_count = db.IntegerProperty(default=0)
	public Integer billing_failure_count = 0;

	// next_billing_date = db.DateProperty()
	@Index public Date next_billing_date;
	// billing_state = db.StringProperty(choices=['paid', 'pending'])
	public String billing_state;

	// consider moving these to a UserSettings model in the future.
	// company_info = db.StringProperty()
	@Index public String company_info;

	// title = db.StringProperty()
	public String title;

	// prefer_demo = db.BooleanProperty (default=False)
	public boolean prefer_demo = false;

	// dsumera - this is already in the place model
	// appdirect_uid = db.StringProperty()
	// clover_id = db.StringProperty()
	public String appdirect_uid;
	public String clover_id;
	
	// dsumera[03/30/16]: used for mailing lists, removing
	// infusionsoft_id = db.IntegerProperty()

	// TODO: should consolidate these two tokens into one
	// used this to track admin access request for firespotter user
	// admin_access_request_token = db.StringProperty()
	// adding this to aid in password reset- A.S
	// password_reset_token = db.StringProperty()
	public String admin_access_request_token;
	public String password_reset_token;

	// options in constants.USERPROFILE_FLAGS
	// flags = db.StringListProperty(indexed=False)
	@IgnoreSave(IfDefault.class)
	public List<String> flags = new ArrayList<String>();

	// promo_code = db.ReferenceProperty(PromoCode, default=None)
	@Load(PromoCode.class)
	public Ref<PromoCode> promo_code;

	// edition_type = db.StringProperty()
	public String edition_type;

	// dsumera[03/30/16]: not sure where this is synthesized
	@Index public transient List<String> __searchable_text_index = new ArrayList<String>();
	
	/** search index for <code>SearchDao</code> */
	@Index
	public transient List<String> __searchable_text_index_first_name_last_name_email_phone_number = new ArrayList<String>();

	// DEPRECATED: do not use
	// moved to Place model
	// is_premium = db.BooleanProperty(default=False)
	// is_parent = db.BooleanProperty (default=False)
	@Index boolean is_premium = false;
	public boolean is_parent = false;

	// parent_User = db.SelfReferenceProperty(default=None)
	@Load(User.class)
	@Index public Ref<User> parent_User;

	// no_of_place = db.IntegerProperty(default=1)
	public int no_of_place = 1;

	// address = db.StringProperty(indexed=False)
	public String address;

	// city = db.StringProperty()
	public String city;

	// region = db.StringProperty()
	public String region;

	// postal_code = db.StringProperty()
	public String postal_code;

	// country = db.StringProperty()
	public String country;

	// appdirect_user = db.BooleanProperty(default=False)
	public boolean appdirect_user = false;

	// corey: If we want to do this, use a StringList with 'self' and 'admin'
	// paid_by = db.IntegerProperty(default=0) # 0 = self , 1= by admin
	public int paid_by = 0;

	// has_restaurant_claimed = db.BooleanProperty(default=False)
	public boolean has_restaurant_claimed = false;

	// billing_off_days = db.IntegerProperty(default=0)
	public int billing_off_days = 0;

	// paid_amount = db.StringProperty()
	public String paid_amount;

	// opt_out = db.BooleanProperty(default=False)
	public boolean opt_out = false;

	// braintree_id = db.BooleanProperty(default=False)
	public boolean braintree_id = false;

	/**
	 * Capitalize and concatenate the name.
	 * 
	 * @return
	 */
	public String fullName() {
		String name = "";

		if (first_name != null) {
			name = first_name.substring(0, 1).toUpperCase();
			if (first_name.length() > 0)
				name += first_name.substring(1);
			name += " ";
		}

		if (last_name != null) {
			name += last_name.substring(0, 1).toUpperCase();
			if (last_name.length() > 0)
				name += last_name.substring(1);
		}

		return name.trim();
	}

	@OnSave
	private void update() {
		// set a new modified date
		modified_date = new Date();
		
		// TODO normalize back to primitive once values are populated
		if (billing_failure_count == null)
			billing_failure_count = 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * Equality determined by key/id comparison.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	/* 
	 * def archive(self): archived_user = UserProfileArchive()
	 * gtools.sync_props(self, archived_user, copy_empty=True)
	 * archived_user.put() self.delete()
	 */
	
	// flags
	
	public static final String WELCOME_SENT = "welcome_email_sent";
}