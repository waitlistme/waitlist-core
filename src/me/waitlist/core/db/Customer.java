package me.waitlist.core.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import scala.actors.threadpool.Arrays;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;

/**
 * <code>Customer</code>-entity associated with a <code>Party</code>.
 * 
 * @author dsumera
 */
@Entity
public class Customer implements Serializable {

	@Id public Long id;

	@Index public Date added_date = new Date();
	@Index public Date modified_date = new Date();
	public Date survey_notified_date;
	
	@Index public String name;
	public String first_name;
	public String last_name;
	
	@Index public String email;
	@Index public String phone_number;
	
	public String city;
	public String region;
	public String postal_code;
	
	public Customer() {
		// default no-arg constructor
	}
	
	public Customer(String name, String phone) {
		this.name = name;
		phone_number = phone;
	}
/*
 *

  @classmethod
  def SearchableProperties(cls):
    return [['first_name', 'last_name', 'email', 'phone_number']]

  def full_name(self):
    if self.first_name:
      name = self.first_name.capitalize()
      if self.last_name:
        name += ' ' + self.last_name.capitalize()
    else:
      name = self.name
    return name
 */
	@Index public transient List<String> __searchable_text_index_first_name_last_name_email_phone_number = new ArrayList<String>();
	
	@OnSave
	public void update() {
		// set a new modified date
		modified_date = new Date();
		
		// update the searchable index
		Set<String> values = new HashSet<String>();
		if (email != null)
			values.addAll(Arrays.asList(email.toLowerCase().split("\\p{Punct}")));
		if (name != null)
			values.addAll(Arrays.asList(name.toLowerCase().split("\\p{Space}")));
		if (first_name != null) values.add(first_name.toLowerCase());
		if (last_name != null) values.add(last_name.toLowerCase());
		if (phone_number != null) values.add(phone_number);
		
		// remove any value that is less than 3 in length
		for (Iterator<String> it = values.iterator(); it.hasNext();)
			if (it.next().length() < 3) it.remove();
		
		// add all values to index
		__searchable_text_index_first_name_last_name_email_phone_number.clear();
		__searchable_text_index_first_name_last_name_email_phone_number.addAll(values);
	}
}