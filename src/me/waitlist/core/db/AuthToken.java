package me.waitlist.core.db;

import java.io.Serializable;
import java.util.Date;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Entity for capturing a <code>AuthToken</code> for api access.
 * 
 * @author dsumera
 */
@Entity
public class AuthToken implements Serializable {

	/** dsumera[03/21/16]: in python codebase this is called 'token', do same here */
	@Id public String token;
	
    /** user associated with this authorization token */
    @Index public Ref<User> user;
    
    /** created date */
    @Index public Date added_date = new Date();
    
    // default to ""
    @Index public String type = "";
    
    public Text session_related_data;
}