package me.waitlist.core.db;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;
import com.googlecode.objectify.annotation.OnSave;

@Entity
//@Cache
public class Party implements Serializable {

	@Id public Long id;
	
	@Load(Place.class)
	@Index public Ref<Place> place;
	
	public String name;
	public String table;
	@Index public int size = 0;
	
	// stats
	public int child_males = 0;
	public int child_females = 0;
	public int adult_males = 0;
	public int adult_females = 0;
	public int old_males = 0;
	public int old_females = 0;
	public int unknown_persons = 0;
	public int child = 0;
	public int adult = 0;
	public int old = 0;

	/** dsumera[04/08/16]: request might have been customer initiated; look at normalizing into Party */
	@Index public Ref<Request> party_request;
	@Index public Ref<Customer> customer;
	
	/** date of most recent notification */
    public Date notify_date;
    @Index public Date added_date = new Date();
    @Index public Date seated_date;
    @Index public Date removed_date;
    @Index public Date modified_date;
    @Index public Date estimated_arrival_time;
	
    // status = db.StringProperty(default='waiting',choices=set(['waiting', 'removed', 'seated']))
    @Index public String status = "waiting";
    
    // in minutes
    @Index public int estimated_wait;
    
    // dsumera[04/04/16]: boxed for nullability
    public Integer actual_wait;
    
    // indicators
    public int color_status = 0;
    public Date color_date;
    
    // settings
    public Text size_allocation_stack;
    // manager_callback_done = db.StringProperty(default='pending',choices=('pending','reject','done'))
    public String manager_callback_done = "pending";
    public boolean api_monitored = false;
    public boolean web_monitored = false;
    public boolean mobile_monitored = false;
    
    /** whether party should be removed after specified deadline */
    @Index public boolean waitlist_auto_removal = true;
    @Index public boolean notified_for_survey = false;
    
    public Text notes;
    public int note_count = 0;
    
    // unique id to find place in public waitlist (i.e., anchor)
    public String short_id;
    
    /** types of notifications party has been sent */
    public List<String> notifications;
    public List<Integer> notification_ids;
    // sms_notification = db.StringProperty(choices=set(['need_a_few_minutes', 'decided_not_to_join']))
    public String sms_notification;
    
    // DEPRECATED: Do not use
    // shortener_url = db.StringProperty()  # migrated to short_id
    // notificationread = db.BooleanProperty(default=False)
    
	/*
  def remaining_waiting_time(self):
    total_second = 0
    if self.estimated_wait :
      added_date = self.added_date + datetime.timedelta(minutes=self.estimated_wait)
      now = datetime.datetime.now()
      if added_date > now:
        time_diff = added_date - now
        total_second = time_diff.seconds
    elif self.estimated_arrival_time and self.estimated_arrival_time > self.added_date:
      time_diff = self.estimated_arrival_time - self.added_date
      total_second = time_diff.seconds
    return total_second
	 */
    
    /**
     * Returns the wait in seconds.
     * 
     * @return
     */
    public int remainingWait() {
    	Calendar cal = Calendar.getInstance();
    	Date now = new Date();
    	
    	// calculate the wait time
    	if (estimated_arrival_time != null)
    		// reservation
    		// dsumera[04/04/16]: this used to be diff'd against the added date
    		cal.setTime(estimated_arrival_time);
    	else {
    		// normal waitlist
    		cal.setTime(added_date);
    		cal.add(Calendar.MINUTE, estimated_wait);
    	}
    	
    	Date wait = cal.getTime();
    	return wait.after(now) ? (int)((wait.getTime() - now.getTime())/1000) : 0;
    }
    
    @OnSave
    private void update() {
    	// set a new modified date
    	modified_date = new Date();
    	
		// update the size of the party
		size = child_males + child_females + adult_males + adult_females
				+ old_males + old_females + unknown_persons + child + adult + old;
    }
}