package me.waitlist.core.db;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Entity for capturing a <code>CreditCard</code>.
 * 
 * @author dsumera
 */
@Entity
public class CreditCard implements Serializable {

	@Id public Long id;
	
    /** user associated with this place */
    @Index public Ref<User> user;
    
    @Index public boolean is_primary;
    @Index public String token;
    @Index public String bin;
    @Index public String expiration_month;
    @Index public String expiration_year;
    @Index public String last_4;
    @Index public String card_type;
    @Index public String cardholder_name;
    @Index public String postal_code;
    
    // created date
    @Index public Date added_date = new Date();
}