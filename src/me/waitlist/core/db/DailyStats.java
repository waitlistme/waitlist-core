package me.waitlist.core.db;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Daily <code>Stats</code> data.
 *
 * @author dsumera
 */
@Entity
public class DailyStats extends Stats implements Serializable {

    @Id public String id;

    public DailyStats() {
    	/* default no-arg constructor */
    }
    
    public DailyStats(String id) {
    	this.id = id;
    }
    
    /**
     * date of the hourly stat
     */
    @Index
    public Date date;
}