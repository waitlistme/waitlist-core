package me.waitlist.core.db;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.io.Serializable;
import java.util.Date;

/**
 * Hourly <code>Stats</code> data.
 *
 * @author dsumera
 */
@Entity
public class HourlyStats extends Stats implements Serializable {

    @Id public String id;

    public HourlyStats() {
    	/* default no-arg constructor */
    }
    
    public HourlyStats(String id) {
    	this.id = id;
    }
    
    /**
     * date of the hourly stat
     */
    @Index public Date date;

    // logic for getting a stats key name (i.e., for the id field)
//    # Gets a unique name for a given place (and possibly time)
//    def _get_place_stat_key_name(place, time=None):
//    place_str = str(place.key().id()) if place else 'all'
//    key_name = 'place-' + place_str
//    if time:
//    time_str = time.strftime('%Y%m%d-%H%M')
//    key_name += '|' + time_str
//    return key_name
}