package me.waitlist.core
package dao

import me.waitlist.core.db._
import com.google.inject.ImplementedBy

// daos
@ImplementedBy(classOf[impl.CustomerDaoImpl])
trait CustomerDao extends GenericDao[Customer] { self: impl.CustomerDaoImpl => }

@ImplementedBy(classOf[impl.PartyDaoImpl])
trait PartyDao extends GenericDao[Party] { self: impl.PartyDaoImpl => }

@ImplementedBy(classOf[impl.UserPlaceDaoImpl])
trait UserPlaceDao extends GenericDao[UserPlace] { self: impl.UserPlaceDaoImpl => }

// searchable daos
@ImplementedBy(classOf[impl.PlaceDaoImpl])
trait PlaceDao extends GenericDao[Place] with SearchDao[Place] {
  // self-typed to specified implementation, can invoke impl methods w/in this trait
  self: impl.PlaceDaoImpl =>
}

@ImplementedBy(classOf[impl.UserDaoImpl])
trait UserDao extends GenericDao[User] with SearchDao[User] { self: impl.UserDaoImpl => }

// stats
@ImplementedBy(classOf[impl.DailyStatsDaoImpl])
trait DailyStatsDao extends GenericDao[DailyStats] with CreateDao[DailyStats] { self: impl.DailyStatsDaoImpl => }

@ImplementedBy(classOf[impl.HourlyStatsDaoImpl])
trait HourlyStatsDao extends GenericDao[HourlyStats] with CreateDao[HourlyStats] { self: impl.HourlyStatsDaoImpl => }

@ImplementedBy(classOf[impl.OverallStatsDaoImpl])
trait OverallStatsDao extends GenericDao[OverallStats] with CreateDao[OverallStats] { self: impl.OverallStatsDaoImpl => }