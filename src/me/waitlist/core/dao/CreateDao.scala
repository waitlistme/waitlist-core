package me.waitlist.core
package dao

import me.waitlist.core.dao.impl.GenericDaoImpl

/**
 * DAO which allows creation of entities which are not found.
 *
 * @author dsumera
 */
trait CreateDao[E] {
  self: GenericDaoImpl[E] =>

  /**
   * Find an entity by <code>Long</code> id, created if doesn't exist.
   * @param id
   * @return
   */
  def findOrCreate(id: Long) = find(id) match {
    case Some(x) => x
    // create entity and return
    case None => persist(newInstance(id)).values.iterator.next
  }

  /**
   * Find an entity by <code>String</code> id, created if doesn't exist.
   * @param id
   * @return
   */
  def findOrCreate(id: String) = find(id) match {
    case Some(x) => x
    // create entity and return
    case None => persist(newInstance(id)).values.iterator.next
  }

  /**
   * Generate a new instance using a <code>Long</code> id.
   * @param id
   * @return
   */
  def newInstance(id: Long): E = null.asInstanceOf[E]

  /**
   * Generates a new instance using a <code>String</code> id.
   * @param id
   * @return
   */
  def newInstance(id: String): E = null.asInstanceOf[E]
}