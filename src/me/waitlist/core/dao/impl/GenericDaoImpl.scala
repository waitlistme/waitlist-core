package me.waitlist.core
package dao.impl

import java.util.Map
import com.google.appengine.api.datastore.Cursor
import com.googlecode.objectify.cmd.Query
import com.googlecode.objectify.{Key, Work}
import me.waitlist.core.gae.WMOfyService.{ofy => o}
import me.waitlist.core.dao.crit._
import scala.collection.JavaConversions._
import com.google.appengine.api.datastore.Entity
import com.google.appengine.api.datastore.Query.FilterOperator
import com.google.appengine.api.datastore.Query.FilterPredicate
import me.waitlist.core.dao.GenericDao

/**
 * Objectify-based implementation of DAO pattern.
 *
 * @author dsumera
 */
abstract class GenericDaoImpl[E](clazz: Class[E]) extends GenericDao[E] {
  //opts: ObjectifyOpts = new ObjectifyOpts)
  //extends DAOBaseBridge[E]

  // register class
  //ObjectifyService.register(clazz)

  override def delete[T: Manifest](ts: T*) = manifest[T] match {
    case m if m == manifest[Long] => o.delete.`type`(clazz).ids(ts).now
    case m if m == manifest[String] => o.delete.`type`(clazz).ids(ts).now
    // otherwise could be Keys or actual entities
    case _ => o.delete.entities(ts).now
  }

  override def find(id: Long): Option[E] = Option(o.load.`type`(clazz).id(id).now)

  override def find(id: String): Option[E] = Option(o.load.`type`(clazz).id(id).now)

  override def find(key: Key[E]): Option[E] = Option(o.load.key(key).now)

  override def find[T: Manifest](ts: T*) = manifest[T] match {
    case m if m == manifest[String] => o.load.`type`(clazz).ids(ts)
    
    // use both cases of Long since type conversions may or may not occur (java vs scala)
    case m if m == manifest[java.lang.Long] => o.load.`type`(clazz).ids(ts)
    case m if m == manifest[Long] => o.load.`type`(clazz).ids(ts)

    // otherwise represents Keys
    case _ => o.load.keys[E](ts.asInstanceOf[Seq[Key[E]]])
  }

  //override def find(ids: String*): Map[String, E] = o.load.`type`(clazz).ids(ids)
  //override def find(ids: Long*): Map[Long, E] = o.load.`type`(clazz).ids(ids)
  //override def find(keys: Key[E]*): Map[Key[E], E] = o.load.keys[E] (keys)
  //override def find(key: Key[E], keys: Key[E]*): Map[Key[E], E] = ofy.get(key :: keys.toList)

  override def find(criteria: Criterion*)(loadGroups: Class[_]*): ResultsCursor[E] = {
    val it = createQuery(criteria: _*)(loadGroups: _*).iterator
    it.toList -> it.getCursor
  }

  // predicate is option filter that cannot be performed through GQL (e.g., distance)
  override def findWithFilter(criteria: Criterion*)(loadGroups: Class[_]*)(implicit predicate: E => Boolean): ResultsCursor[E] = {
    val it = createQuery(criteria: _*)(loadGroups: _*).iterator
    // should we use "withFilter" here? not necessarily, withFilter aliases filter for iterators
    it.filter(predicate).toList -> it.getCursor
  }

  override def findAll: java.util.List[E] = o.load.`type`(clazz).list

  override def findKeys(criteria: Criterion*): ResultsCursor[Key[E]] = {
    // retrieve iterator to also access cursor
    val it = createQuery(criteria: _*)().keys.iterator
    it.toList -> it.getCursor
  }
  
  override def findKeysWithFilter(criteria: Criterion*)(implicit filter: Key[E] => Boolean): ResultsCursor[Key[E]] = {
    val it = createQuery(criteria: _*)().keys.iterator
    it.filter(filter).toList -> it.getCursor
  }

  // pair of parent map w/ cursor
  override def findParents[P](criteria: Criterion*): Map[Key[P], P] Tuple2 String = {
    //createQuery(criteria: _*).fetchParents[P] ->
    // retrieve parent keys and load parent entities into map
    val it = createQuery(criteria: _*)().keys.iterator
    // force evaluation of iterator to move position of cursor
    val keys = it.map(_.getParent.asInstanceOf[Key[P]]).toList
    // need filter for find operation below
    //implicit def passthru = (_: E) => true
    o.load.keys[P](keys) -> it.getCursor //find(criteria: _*)()._2
    //createQuery(criteria: _*).fetchParents[P] -> find(criteria: _*)._2
  }

  //  // pair of parent map w/ cursor
  //  override def findParents[P](criteria: Criterion*): Map[Key[P], P] Tuple2 String = {
  //    //createQuery(criteria: _*).fetchParents[P] ->
  //    implicit def passthru = (_: E) => true
  //    val keys = createQuery(criteria: _*)().keys.map(_.getParent.asInstanceOf[Key[P]])
  //    o.load.keys(keys.toSeq) -> find(criteria: _*)()._2
  //    //createQuery(criteria: _*).fetchParents[P] -> find(criteria: _*)._2
  //  }

  /**
   * Must include ordering when using an inequality filter (use ascription to cast LoadType as Query).
   * @param criteria
   * @param loadGroups
   * @return
   */
  private def createQuery(criteria: Criterion*)(loadGroups: Class[_]*): Query[E] =
    ((o.load.group(loadGroups: _*).`type`(clazz): Query[E]) /: criteria)((q, c) => c match {
      case AncestorCriterion(a) => Option(a).toRight(q).fold(identity, q ancestor _)
      case BetweenCriterion(p, l, h) => Option(p).toRight(q).fold(identity,
          // inclusive lower bound, exclusive upper bound
          p => q.order(p).filter(p concat " >=", l).filter(p concat " <", h))
      case CursorCriterion(c) => Option(c).toRight(q).fold(identity, q startAt Cursor.fromWebSafeString(_))
      case EQCriterion(p, v) => Option(p).toRight(q).fold(identity, q.filter(_, v))
      case EQRawKeyCriterion(p, k) => Option(p).toRight(q).fold(identity, p => q.filter(new FilterPredicate(p, FilterOperator.EQUAL, k)))
      case GTCriterion(p, v) => q.order(p).filter(p concat " >", v)
      case GTECriterion(p, v) => q.order(p).filter(p concat " >=", v)
      case InCriterion(p, vs) if !vs.isEmpty => q.filter(p concat " in", vs)
      //case InCriterionJ(p, vs) if !vs.isEmpty => q.filter(p concat " in", vs)
      //case KeyCriterion(l) => q.order("__key__").filter("__key__ >=", new Key(clazz, l)).filter("__key__ <=", new Key(clazz, l concat "\ufffd"))
      case KeyCriterion(l) => q.order("__key__").filterKey(">=", l).filterKey("<=", l concat "\ufffd")
      case LTCriterion(p, v) => q.order(p).filter(p concat " <", v)
      case LTECriterion(p, v) => q.order(p).filter(p concat " <=", v)
      case LimitCriterion(l) => q limit l
      case NECriterion(p, v) => Option(p).toRight(q).fold(identity, s => q.filter(s concat " !=", v))
      case NotZeroCriterion(p) => q.order(p).filter(p concat " >", 0)
      case OffsetCriterion(o) => q offset o
      case SortCriterion(s) if (s != "") => q order s
      case StartsWithCriterion(p, v) => q.order(p).filter(p concat " >=", v).filter(p concat " <=", v concat "\ufffd")
      case _ => q
    })

  override def persist(es: E*): Map[Key[E], E] = o.save().entities(es: _*).now()

  //override def transact(task: GenericDao[E] => Unit) = o.transact(
  //  new Work[Unit] {
  //    override def run = task(GenericDaoImpl.this)
  //  })

  //  override def transact(task: (GenericDao[E]) => Unit) {
  //    while (true)
  //      try {
  //        // create new dao to perform task within
  //        new GenericDaoImpl[E](clazz, new ObjectifyOpts().setBeginTransaction(true)) {
  //          // executes the task in the transactional context of this DAO/ofy
  //          def doTransaction(task: (GenericDao[E]) => Unit) = try {
  //            task(this)
  //            this.ofy.getTxn.commit
  //          } finally
  //            if (this.ofy.getTxn.isActive) this.ofy.getTxn.rollback
  //        }.doTransaction(task)
  //
  //        // if no error, just return (break out of while loop)
  //        return
  //      } catch {
  //        case e => warning("could not perform transaction: " + e.getClass.getName)
  //      }
  //  }

  override def transact[R](task: GenericDao[E] => R) = o.transact(task)

  /**
   * Return serializable version of <code>Cursor</code> for web response.
   * @param c
   * @return
   */
  implicit def cursor2String(c: Cursor): String = Option(c) match {
    case Some(_) => c.toWebSafeString
    case None => null
  }

  /**
   * Conversion of anonymous function to <code>Work</code> instance. Alternatively, consider using
   * <code>@Transact</code> annotation.
   * @param f
   * @tparam R
   * @return
   */
  implicit def func2Work[R](f: GenericDao[E] => R): Work[R] = new Work[R] {
    override def run = f(GenericDaoImpl.this)
  }
}