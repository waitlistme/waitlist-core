package me.waitlist.core
package dao.impl

import me.waitlist.core.db._
import me.waitlist.core.dao._

class CustomerDaoImpl extends GenericDaoImpl(classOf[Customer]) with CustomerDao
class PartyDaoImpl extends GenericDaoImpl(classOf[Party]) with PartyDao
class UserPlaceDaoImpl extends GenericDaoImpl(classOf[UserPlace]) with UserPlaceDao

/** <code>Place</code> dao */
class PlaceDaoImpl extends {
  val properties = List("name")
  val spec = "PlaceIndex"
} with GenericDaoImpl(classOf[Place]) with PlaceDao

/** <code>User</code> dao */
class UserDaoImpl extends {
  val properties = List("first_name", "last_name", "email", "phone_number")
  val spec = "UserIndex"
} with GenericDaoImpl(classOf[User]) with UserDao

// stats daos
class DailyStatsDaoImpl extends GenericDaoImpl(classOf[DailyStats]) with DailyStatsDao {
  override def newInstance(id: String): DailyStats = new DailyStats(id)
}
class HourlyStatsDaoImpl extends GenericDaoImpl(classOf[HourlyStats]) with HourlyStatsDao {
  override def newInstance(id: String): HourlyStats = new HourlyStats(id)
}
class OverallStatsDaoImpl extends GenericDaoImpl(classOf[OverallStats]) with OverallStatsDao {
  override def newInstance(id: String): OverallStats = new OverallStats(id)
}