package me.waitlist.core
package dao

import java.util.Map

import com.googlecode.objectify.Key
import me.waitlist.core.dao.crit.Criterion

/**
 * Base trait for DAOs.
 * @tparam E
 *
 * @author dsumera
 */
trait GenericDao[E] {
  // types
  //type E // entity
  //type ID // id for key

  /** combine results list w/ cursor */
  type ResultsCursor[A] = (List[A], String)

  /**
   * Deletion of multiple entities (through entities, ids or <code>Key</code>s).
   * @param ts entities, ids or <code>Key</code>s
   */
  def delete[T: Manifest](ts: T*): Unit

  /**
   * Retrieve entity by <code>Long</code> id.
   * @param id
   * @return
   */
  def find(id: Long): Option[E]

  /**
   * Retrieve entity by <code>String</code> id.
   * @param id
   * @return
   */
  def find(id: String): Option[E]

  /**
   * Retrieve entity by its <code>Key</code> reference.
   * @param key
   * @return
   */
  def find(key: Key[E]): Option[E]

  /**
   * Retrieve multiple entities (through ids or <code>Key</code>s).
   * @param ts
   * @tparam T
   * @return
   */
  def find[T: Manifest](ts: T*): Map[_, E]

  //  def find(ids: String*): Map[String, E]
  //  def find(ids: Long*): Map[Long, E]

  //  /**
  //   * Retrieve a <code>Map</code> of entities through variable <code>Key</code>s.
  //   * @param keys
  //   * @return
  //   */
  //  def find(keys: Key[E]*): Map[Key[E], E]

  //def find(key: Key[E], keys: Key[E]*): Map[Key[E], E]

  /**
   * General find method using <code>Criterion</code> and load groups.
   * @param criteria
   * @param loadGroups
   * @return
   */
  def find(criteria: Criterion*)(loadGroups: Class[_]*): ResultsCursor[E]

  /**
   * General find method using variable <code>Criterion</code>, load groups and predicate filtering.
   * @param criteria
   * @param loadGroups
   * @param filter filter applied to each entity
   * @return a <code>Tuple2</code> of a <code>List</code> of entities and a <code>Cursor</code> as web-safe string
   */
  def findWithFilter(criteria: Criterion*)(loadGroups: Class[_]*)(implicit filter: E => Boolean): List[E] Tuple2 String

  /**
   * Retrieve a <code>List</code> of all entities.
   * @return
   */
  def findAll: java.util.List[E]

  /**
   * Retrieve all <code>Key</code>s of entities satisfying variable <code>Criterion</code>.
   * @param criteria
   * @return a <code>Tuple2</code> of a <code>List</code> of <code>Key</code>s and a <code>Cursor</code> as web-safe string
   */
  def findKeys(criteria: Criterion*): List[Key[E]] Tuple2 String

  
  /**
   * Retrieve all <code>Key</code>s of entities satisfying variable <code>Criterion</code> and filtering.
   * @param criteria
   * @param filter filter applied to each <code>Key</code>
   * @return a <code>Tuple2</code> of a <code>List</code> of <code>Key</code>s and a <code>Cursor</code> as web-safe string
   */
  def findKeysWithFilter(criteria: Criterion*)(implicit filter: Key[E] => Boolean): List[Key[E]] Tuple2 String

  /**
   * Retrieve all parents of entities satisfying variable <code>Criterion</code>.
   * @param criteria
   * @tparam P
   * @return <code>Tuple2</code> of <code>Map</code> of parents of matching entities and a <code>Cursor</code> as web-safe string;
   *         note the cursor applies to the <i>fetched child entities</i>, <b>not</code> the parents
   */
  def findParents[P](criteria: Criterion*): Map[Key[P], P] Tuple2 String

  /**
   * Save variable entities against the datastore.
   * @param es
   * @return
   */
  def persist(es: E*): Map[Key[E], E]

  /**
   * Encapsulate persistence operation in a transactional context, similar to using <code>Transact</code> annotation.
   * Repeats task in transaction until successful completion.
   * @param task
   * @tparam R
   */
  def transact[R](task: GenericDao[E] => R)
}