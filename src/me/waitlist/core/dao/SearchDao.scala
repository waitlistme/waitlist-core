package me.waitlist.core
package dao

import java.util.logging.Logger
import scala.collection.JavaConversions.collectionAsScalaIterable
import scala.collection.JavaConversions.iterableAsScalaIterable
import com.google.appengine.api.search.Cursor
import com.google.appengine.api.search.Index
import com.google.appengine.api.search.IndexSpec
import com.google.appengine.api.search.Query
import com.google.appengine.api.search.QueryOptions
import com.google.appengine.api.search.SearchService
import javax.inject.Inject
import java.util.Date
import com.google.appengine.api.search.Document
import com.google.appengine.api.search.Field

/**
 * DAO which incorporates a keyword searchable aspect to datastore operations.
 *
 * @author dsumera
 */
trait SearchDao[E] {
  self: impl.GenericDaoImpl[E] =>

  /** abstract properties must be overriddden/provided */
  val properties: List[String]

  /** initialize the array list index upon creation */
  val fullText = ("__searchable_text_index" :: properties).mkString("_")

  /** name of the document index */
  val spec: String

  /** document search index for the associated entity */
  private[this] var index: Index = _

  @Inject private[this] var log: Logger = _

  /**
   * Using search service setter injection to initialize the search index.
   */
  @Inject
  def setSearchService(searchServ: SearchService) =
    index = searchServ.getIndex(IndexSpec.newBuilder.setName(spec).build)

  /** creating index from name and search service */
  //val spec = searchServ.getIndex(IndexSpec.newBuilder.setName("PlaceIndex").build)

  /**
   * Search method to find entities which possess a property value matching the given query.
   * @param query
   * @param limit
   * @return
   */
  def search(query: String, limit: Int) = {
    // TODO need to change this to something we can manipulate
    //val spec = searchServ.getIndex(IndexSpec.newBuilder.setName(index).build)
    val options = QueryOptions.newBuilder.setCursor(Cursor.newBuilder.build)
    val q = Query.newBuilder.setOptions(options).build(s"name_tokens: $query")
    // send a warning to the logs
    //log.warning(s"query is: name_tokens: $query")
    val rv = index.search(q)

    // TODO check when this cursor is valid or not
    find(rv.map(_.getId.toLong).toList: _*).values.toList -> "" //rv.getCursor.toWebSafeString 
    //val crit = LimitCriterion(limit) :: query.split(" ").map(GTECriterion(fullText, _)).toList
    //find(/*GTECriterion(fullText, query)*/crit: _*)(/* no load classes */)
  }

  def insertToken {

    //      new_doc = fts.Document(
    //    doc_id=unicode(place.key().id()),
    //    fields=[
    //      fts.TextField(name='place_id', value=_encode_txt(place.key().id())),
    //      fts.TextField(name='name', value=_encode_txt(place.name)),
    //      fts.TextField(name='name_tokens', value=_encode_txt(name_token_str)),
    //      fts.TextField(name='phone_tokens', value=_encode_txt(phone_token_str)),
    //      fts.TextField(name='email_tokens', value=_encode_txt(email_token_str)),
    //      ]
    //    )

    val doc = Document.newBuilder
      //.setId() // Setting the document identifer is optional. If omitted, the search service will create an identifier.
      .addField(Field.newBuilder.setName("name") setText "Dennis")
      .addField(Field.newBuilder.setName("name_tokens") setText "shrimp catfish hotdogs")
      .addField(Field.newBuilder.setName("phone_tokens") setText "2106517453")
      .addField(Field.newBuilder.setName("email_tokens") setText "dennis@waitlist.me")
      .addField(Field.newBuilder.setName("place_id") setText "29902")
      //.addField(Field.newBuilder.setName("name").set("the rain in spain"))
      //.addField(Field.newBuilder().setName("email")
      //    .setText(currentUser.getEmail()))
      //.addField(Field.newBuilder().setName("domain")
      //    .setAtom(currentUser.getAuthDomain()))
      //.addField(Field.newBuilder.setName("published").setDate(new Date()))
      .build

    // save the document
    index put doc
  }
}