package me.waitlist.core
package config

import com.googlecode.objectify.ObjectifyFilter
import com.sun.jersey.api.core.ResourceConfig
import com.sun.jersey.api.core.PackagesResourceConfig
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer
import scala.collection.JavaConversions._

/**
 * Servlet configuration for establishing filters and servlets (e.g., RESTful endpoints).
 *
 * @author dsumera
 */
abstract class ServletModule extends com.google.inject.servlet.ServletModule {

  override def configureServlets {

    // filters

    // process all calls through ObjectifyFilter 
    // clean up any thread-local transaction contexts and pending asynchronous operations that
    // remain at the end of a request

    // filter("/*").through(AppstatsFilter.class, appstatsParams);
    filter("/*") through classOf[ObjectifyFilter]

    // NOTE: any servlet mappings in deployment descriptor will be overridden by configurations
    // defined in this method; ensure these servlets are also defined in the ServletModule

    //    // configure guice resources
    //    serve("/*") `with` (classOf[GuiceContainer],
    //
    //      // configuration parameters
    //      Map(
    //        // where to find root resources
    //        PackagesResourceConfig.PROPERTY_PACKAGES -> packages,
    //
    //        // disable WADL
    //        ResourceConfig.FEATURE_DISABLE_WADL -> "false", // pojo mapping - MANDATORY
    //        
    //        //JSONConfiguration.FEATURE_POJO_MAPPING -> true,
    //        "com.sun.jersey.api.json.POJOMappingFeature" -> "true"
    //        ))
  }
}