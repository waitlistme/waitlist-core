package me.waitlist.core
package config

import com.google.inject.AbstractModule
import me.waitlist.core.gae.WMOfyService
import me.waitlist.core.gae.txn.Transact
import me.waitlist.core.gae.txn.TransactInterceptor
import com.googlecode.objectify.ObjectifyFilter
import com.google.inject.Provides
import javax.inject.Singleton
import com.google.appengine.api.search.SearchServiceFactory
import me.waitlist.core.rs.ext.ObjectMapperProvider
import com.google.inject.matcher.Matchers
import com.google.appengine.api.memcache.MemcacheServiceFactory

/**
 * Server module for Guice initialization of server component graph. Every dependency must be bound within
 * this component (especially when utilizing annotation-based bindings).
 *
 * @author dsumera
 */
class ServerModule extends AbstractModule {

  override def configure {
    // statically inject WMOfyService (through static setters)
    requestStaticInjection(classOf[WMOfyService])

    // enable the @Transaction annotation
    bindInterceptor(Matchers.any, Matchers.annotatedWith(classOf[Transact]), new TransactInterceptor)

    // use jackson serialization for jaxrs; is binding necessary?
    bind(classOf[ObjectMapperProvider])

    // external things that don't have Guice annotations
    //bind(AppstatsFilter.class).in(Singleton.class)
    //bind(AppstatsServlet.class).in(Singleton.class)
    bind(classOf[ObjectifyFilter]) in (classOf[Singleton])
  }

  // singleton provider injections
  
  @Provides
  @Singleton
  def provideMemcacheService = MemcacheServiceFactory.getAsyncMemcacheService
  
  @Provides
  @Singleton
  def provideSearchService = SearchServiceFactory.getSearchService
}