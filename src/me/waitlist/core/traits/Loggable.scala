package me.waitlist.core
package traits

import java.util.logging.Logger

/**
 * Logging behavior. All classes which mix this trait must set the <code>logClass</code> abstract value through
 * early initialization.
 *
 * @author dsumera
 */
trait Loggable {

  // use log directly to preserve logging context
  val log: Logger = Logger.getLogger(getClass.getName)

  def info(msg: String) = log.info(msg)

  def warning(msg: String) = log.warning(msg)

  def severe(msg: String) = log.severe(msg)

  def throwing(clazz: String, method: String, t: Throwable) = log.throwing(clazz, method, t)
}