package me.waitlist.core
package traits

/**
 * Trait for telephony based checks.
 * 
 * @author dsumera
 */
trait Telephony {
  
  /**
   * Determine whether provided phone number is valid.
   */
  def isNumber(ph: String) = ph match {
    case Telephony.REphone(area, co, line, _) => true
    case _ => false
  }
  
  def normalize(ph: String) = {
    val phone = ph.trim.replaceAll("[\\(\\)\\s\\-\\.\\+]", "")
    if (phone.length == 11 && phone(0) == '1') Option(phone.substring(1))
    else if (phone.isEmpty) None
    else Option(phone)
  }
}

object Telephony {
  val REphone = "(\\d{3})\\W*(\\d{3})\\W*(\\d{4})\\W*(\\d*)"r
}