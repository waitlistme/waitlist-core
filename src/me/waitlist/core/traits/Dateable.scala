package me.waitlist.core.traits

import java.util.TimeZone
import java.util.Calendar
import java.text.SimpleDateFormat

trait Dateable {
  
  /**
   * Get midnight for the current day.
   */
  def getMidnight = {
    val cal = Calendar.getInstance(Dateable.UTC)
    cal.set(Calendar.HOUR, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    cal
  }
  
  def getNow = Calendar.getInstance(Dateable.UTC)
  
  def getSimpleFormat = new SimpleDateFormat("yyyyMMdd-HHmm")
}

object Dateable {
  
  val UTC = TimeZone.getTimeZone("UTC")
}