package me.waitlist.core.traits

/**
 * Trait for classes which require <code>String</code> manipulation.
 * 
 * @author dsumera
 */
trait Textable {
  
  implicit class RichString(s: String) {
    
    /** capitalize all separate words, lowercase everything else */
    def cap = s.trim.toLowerCase.split(' ').map(_.capitalize).mkString(" ")
  }
}