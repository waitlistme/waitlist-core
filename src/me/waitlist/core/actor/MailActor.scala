package me.waitlist.core
package actor

import me.waitlist.core.actor.MailActor._
import java.io.PrintWriter
import java.io.StringWriter

/**
 * Mail module interface.
 */
class MailActor extends TaskActor[MailEvent] {
  
  private val task = taskAsync("mail") _
  
  def receive = {
    case SendBeta(place) => task("/task/send/beta")(Seq("place" -> place.toString))
    case SendClover(user, password) => task("/task/send/clover")(Seq("user" -> user.toString, "password" -> password))
    case SendError(error) =>
      // convert throwable to a string
      val errors = new StringWriter
      error.printStackTrace(new PrintWriter(errors))
      // ensure line separators are included
      task("/task/send/error")(Seq("error" -> errors.toString.replace(System.getProperty("line.separator"), "<br/>\n")))
    case SendLimit(place) => task("/task/send/limit")(Seq("place" -> place.toString))
    case SendReset(user, url) => task("/task/send/reset")(Seq("user" -> user.toString, "url" -> url))
    //case SendStats => task("/task/send/stats")(Nil)
    case SendTips(user) => task("/task/send/tips")(Seq("user"-> user.toString))
    case SendWelcome(user) => task("/task/send/welcome")(Seq("user" -> user.toString))
  }
}

object MailActor {
  abstract sealed trait MailEvent
  case class SendBeta(place: Long) extends MailEvent
  case class SendClover(user: Long, password: String) extends MailEvent
  case class SendError(error: Throwable) extends MailEvent
  case class SendLimit(place: Long) extends MailEvent
  case class SendReset(user: Long, url: String) extends MailEvent
  //case object SendStats extends MailEvent
  case class SendTips(user: Long) extends MailEvent
  case class SendWelcome(user: Long) extends MailEvent
}