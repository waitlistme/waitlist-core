package me.waitlist.core
package actor

import me.waitlist.core.actor.TelephonyActor._

/**
 * Telephony module interface.
 */
class TelephonyActor extends TaskActor[TelephonyEvent] {

  private val task = taskAsync("telephony") _

  def receive = {
    case SendAdd(customer, place, msg) => task("/tasks/send_sms")(Seq(
      "customer_id" -> customer.toString,
      "place_id" -> place.toString,
      "message" -> msg,
      "msg_type" -> "waitlist_add"))
    case SendDeclined(customer, place, msg) => task("/tasks/send_sms")(Seq(
      "customer_id" -> customer.toString,
      "place_id" -> place.toString,
      "message" -> msg,
      "msg_type" -> "rejected"))
    case SendReady(customer, place, msg) => task("/tasks/send_sms")(Seq(
      "customer_id" -> customer.toString,
      "place_id" -> place.toString,
      "message" -> msg,
      "msg_type" -> "table_ready"))
    case SendRemove(customer, place, msg) => task("/tasks/send_sms")(Seq(
      "customer_id" -> customer.toString,
      "place_id" -> place.toString,
      "message" -> msg,
      "msg_type" -> "waitlist_remove"))
  }
}

object TelephonyActor {
  abstract sealed trait TelephonyEvent
  // added to the waitlist
  case class SendAdd(customer: Long, place: Long, msg: String) extends TelephonyEvent
  // declined
  case class SendDeclined(customer: Long, place: Long, msg: String) extends TelephonyEvent
  // ready for customer
  case class SendReady(customer: Long, place: Long, msg: String) extends TelephonyEvent
  // removed from waitlist
  case class SendRemove(customer: Long, place: Long, msg: String) extends TelephonyEvent
}