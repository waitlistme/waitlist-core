package me.waitlist.core
package actor

import me.waitlist.core.actor.StatsActor.StatsEvent
import me.waitlist.core.actor.StatsActor.UpdateDaily
import me.waitlist.core.actor.StatsActor.UpdateHourly

/**
 * Stats module interface.
 */
class StatsActor extends TaskActor[StatsEvent] {
  
  private val task = taskAsync("stats") _
  
  def receive = {
    case UpdateDaily(params @ _*) => task("/task/update/daily")(params)
    case UpdateHourly(params @ _*) => task("/task/update/hourly")(params)
  }
}

object StatsActor {
  abstract sealed trait StatsEvent
  case class UpdateDaily(params: Tuple2[String, String]*) extends StatsEvent
  case class UpdateHourly(params: Tuple2[String, String]*) extends StatsEvent
}