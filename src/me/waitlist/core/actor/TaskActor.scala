package me.waitlist.core
package actor

import com.google.appengine.api.taskqueue.QueueFactory
import com.google.appengine.api.taskqueue.TaskOptions
import com.google.appengine.api.taskqueue.TaskOptions.Builder._

/**
 * Loose translation of Actor-driven/akka style.
 *
 * @author dsumera
 */
abstract class TaskActor[E] {

  /** type alias for receiver */
  type Receiver = PartialFunction[E, Unit]

  /** definition should be exhaustive */
  def receive: Receiver

  /** send an event to the actor */
  def !(event: E) = receive(event)

  /**
   * Asynchronously task specified queue with provided webhook and parameters.
   */
  def taskAsync(queue: String)(webhook: String)(params: Tuple2[String, String]*) = {
    val options = withUrl(webhook)
    for ((k, v) <- params) options.param(k, v)
    QueueFactory.getQueue(queue).addAsync(options)
  }
}