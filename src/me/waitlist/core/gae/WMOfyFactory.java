package me.waitlist.core.gae;

import java.util.logging.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;

import me.waitlist.core.db.AuthToken;
import me.waitlist.core.db.CreditCard;
import me.waitlist.core.db.Customer;
import me.waitlist.core.db.DailyStats;
import me.waitlist.core.db.HourlyStats;
import me.waitlist.core.db.OverallStats;
import me.waitlist.core.db.Party;
import me.waitlist.core.db.Place;
import me.waitlist.core.db.PromoCode;
import me.waitlist.core.db.Request;
import me.waitlist.core.db.User;
import me.waitlist.core.db.UserPlace;

import com.google.inject.Injector;
import com.googlecode.objectify.ObjectifyFactory;

/**
 * Our version of ObjectifyFactory which integrates with Guice. You could add
 * convenience methods here too.
 *
 * @author Jeff Schnitzer
 * @author dsumera
 */
@Singleton
public class WMOfyFactory extends ObjectifyFactory {

	/**
	 * Logger to determine the time for classes to register through Objectify.
	 */
	private static final Logger log = Logger.getLogger(WMOfyFactory.class
			.getName());

	final private Injector injector;

	@Inject
	public WMOfyFactory(Injector injector) {
		this.injector = injector;

		// report duration
		long millis = System.currentTimeMillis();
		register();
		millis = System.currentTimeMillis() - millis;

		log.info("Entity registration: " + millis + " ms");
	}

	/**
	 * Using guice to make instances instead!
	 */
	@Override
	public <T> T construct(Class<T> type) {
		return injector.getInstance(type);
	}

	// /**
	// * Static <code>ofy()</code> call on <code>ObjectifyService</code> invokes
	// this method.
	// *
	// * @return
	// */
	// @Override
	// public Objectify begin() {
	// // return a new instance of the Objectify data interface set with this
	// factory
	// return new WLMOfy(this);
	// }

	/**
	 * Perform registration of db model types.
	 */
	public void register() {
		register(AuthToken.class);
		register(CreditCard.class);
		register(Customer.class);
		register(Party.class);
		register(Request.class);
		register(Place.class);
		register(PromoCode.class);
		register(User.class);
		register(UserPlace.class);

		// stats
		register(DailyStats.class);
		register(HourlyStats.class);
		register(OverallStats.class);
	}
}