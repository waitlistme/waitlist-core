package me.waitlist.core.gae;

import javax.inject.Inject;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

/**
 * Gives us our custom version rather than the standard Objectify one.  Also responsible for setting up the static
 * OfyFactory instead of the standard ObjectifyFactory - make sure to register this class for static injection in
 * Guice.
 *
 * @author Jeff Schnitzer
 * @author dsumera
 */
public class WMOfyService {

    @Inject
    public static void setObjectifyFactory(WMOfyFactory factory) {
        ObjectifyService.setFactory(factory);
    }

    /**
     * @return our extension to Objectify
     */
//    public static WLMOfy ofy() { return (WLMOfy) ObjectifyService.ofy(); }
    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    /**
     * @return our extension to ObjectifyFactory
     */
//    public static WMOfyFactory factory() {
//        return (WMOfyFactory) ObjectifyService.factory();
//    }
    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }
}